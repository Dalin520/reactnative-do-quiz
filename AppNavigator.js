import {createNavigationContainer,createStackNavigator} from 'react-navigation'
import HomeScreen from './HomeScreen';
import ResultScreen from './ResultScreen';


const navigator = createStackNavigator({
    Home : HomeScreen,
    Result: ResultScreen
})

const AppNavigator = createNavigationContainer(navigator)

export default AppNavigator