import React, { Component } from 'react'
import {View, StyleSheet} from 'react-native'
import {RadioGroup,RadioButton} from 'react-native-flexi-radio-button'
import {Container, Content, Card, CardItem, Button, Text} from 'native-base'

export default class HomeScreen extends Component {
    constructor(){
        super()
        this.state=({
          text: '',
          questions: [
              {
                  "text": "What is the name of the US president?", 
                  "correct": 1, 
                  "answers": [
                      "Obama", 
                      "Trump", 
                      "Roosvelt", 
                      "Putin"
                  ]
              }, 
              {
                  "text": "What is the square root of 100?", 
                  "correct": 3, 
                  "answers": [
                      "sin(10)", 
                      "1", 
                      "100", 
                      "10"
                  ]
              },
              {
                  "text": "Which one is correct team name in NBA?",
                  "correct": 3,
                  "answers":[
                    "New York Bulls",
                    "Los Angeles Kings",
                    "Golden State Warriros",
                    "Huston Rocket"
                  ]
              },
              {
                "text": "Which one is correct team name in NBA?",
                "correct": 3,
                "answers":[
                  "New York Bulls",
                  "Los Angeles Kings",
                  "Golden State Warriros",
                  "Huston Rocket"
                ]
            }
          ],
          result : [],
        }
        )
        
    }
    static navigationOptions = {
      title: 'Do Quiz',
      headerStyle: {
        backgroundColor: 'green',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    };
 
    onSelect=(answer,question)=>{
        console.log('answer=',answer);
        console.log('q=',question);
        let selected=this.state.result
        selected[question]=answer
        this.setState({
          result:selected
        })
         console.log('===',selected)
    }
    onSubmit=()=>{
      var score=0;
      var numOfQestion = 0
      var correct = 0;
      for(let x in  this.state.result){
        if(this.state.questions[x].correct == this.state.result[x]){
          score++
        }
      }
      numOfQestion = this.state.questions.length
      correct = Math.round((score/numOfQestion)*100);
      this.props.navigation.navigate("Result",{result: score,numerofq:numOfQestion,correctness:correct})  
    }
  render() {
    return (
      <Container>
        <Text style={{fontSize:20}}>Questions</Text>
        <Content padder>
          <Card>
               {
                this.state.questions.map((question,i) => (
                 <View key={i}  style={styles.container}>                      
                   <Text>{question.text}</Text>
                   <RadioGroup
                       size={24}
                       thickness={2}
                       onSelect = {(index, value) => this.onSelect(index, i)}
                       >
                       {question.answers.map(answer => (                           
                          <RadioButton key={answer} value={answer} color='green'>
                              <Text>{answer}</Text>
                          </RadioButton>                              
                       ))}
                   </RadioGroup>

                 </View>
                ))
               }
               
          </Card>
             
          <Button rounded success style={styles.buttonStyle}
             onPress={()=>{this.onSubmit()}
          }
          >
            <Text>Submit</Text>
          </Button>

        </Content>
      </Container>
    )
  }
}
let styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        padding:10,
    },
    text: {
        padding: 10,
        fontSize: 14,
    },
    buttonStyle: {
        marginTop: 10,
        alignSelf: 'center'
    }
})