import React, { Component } from 'react'
import {Container, Content, Card, CardItem, Button, Text} from 'native-base'


export default class ResultScreen extends Component {
  render() {
    
    console.log('result screen===',this.props)
    const {result,numerofq,correctness} = this.props.navigation.state.params
    return (
      <Container>
        <Content>
          <Card style={{flex:1,justifyContent:'center',alignItems:'center'}}>
              <Text style={{fontSize:30}}>Your Result</Text>
              <Text>Total score: {result}/{numerofq}</Text>
              <Text style={{fontSize:40,color:'green'}}>{correctness}%</Text>
          </Card>
        </Content>
      </Container>
    )
  }
}
